package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.*;

public class BattleGame extends CardGame {
    /**
     * Instancie le jeu Bataille
     *
     * @param maxPlayer nombre maximum de participants
     */
    public BattleGame(Integer maxPlayer) {
        super("Battle", maxPlayer, "resources/games/cardGame.txt");
    }

    /**
     * @return
     * @param playerSockets
     */
    @Override
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        if (players.size() == 0 || players.size() == 1) return null;//si il n'y a pas assez de joueurs
        Collections.shuffle(this.cards);//mélange le paquet
        Player matchWinner = null;//vainqueur du jeu
        Player winner = null;//vainqueur de la manche
        numberOfRounds = 0;//numéro du tour
        int joueur = 0;//numéro du joueur
        for (Card card : this.cards) { //distibue les cartes
            players.get(joueur).addComponent(card); //ajoute la carte du haut du paquet au joueur
            joueur = ++joueur % players.size(); //change de joueur
        }
        displayState(); //affiche l'état du jeu

        while (!end()) { //tant que le jeu n'est pas fini
            numberOfRounds++; //change de tour
            System.out.println("Round " + numberOfRounds);//affiche le tour en cours
            if (numberOfRounds % 10 == 0) {
                //mélange les cartes de chaque joueur en possédant tout les 10 tours
                for (Player player : players) {
                    if (player.getScore() != 0) player.shuffleHand();
                    //mélange les cartes du joueur si il en a
                }
            }

            List<Card> playedCards = new ArrayList<>();//Liste des cartes jouées
            Map<Player, Card> equalPlayedCards = new HashMap<>();
            //Liste des potentiels gagnants de la manche en égalité et la carte qu'ils ont joué
            Integer val = 0;//variable de test pour la valeur des cartes
            for (Player player : players) {
                if (player.getScore() != 0) { //vérifie que le joueur a au moins une carte
                    ObjectInputStream inputStream = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    player.play(playerSockets.get(player));//joue la première carte du joueur
                    Card card = (Card) inputStream.readObject();//récupère la carte jouée
                    card.setHidden(false);//la met face visible
                    if (card.getValue() > val) {//si la carte est la plus forte
                        val = card.getValue();//modifie la valeur de val pour la valeur de la carte
                        winner = player;
                        //met temporairement le joueur l'ayant posé en tant que gagnant pour la manche
                        equalPlayedCards.clear();//vide equalPlayedCards
                        equalPlayedCards.put(player, card);
                        //met le potentiel gagnant de manche et sa carte dans equalPlayedCards
                    } else if (card.getValue() == val) { //si il y a égalité
                        equalPlayedCards.put(player, card); //rajoute le joueur en égalité et sa carte
                        winner = null; //met le gagnant de manche comme indéfinit
                    }
                    playedCards.add(card);//ajoute la carte à la liste des cartes jouées
                    System.out.println(player.getName() + " play " + card.getName());
                    //affiche un message pour dire que le joueur a joué sa carte
                }
            }

            Map<Player, Card> reEqualPlayedCards = new HashMap<>();
            //Liste des potentiels gagnants de la manche à nouveau en égalité et la carte qu'ils ont joué
            while (equalPlayedCards.size() > 1) { //en cas d'égalité
                val = 0;//réinitialise la variable de test pour la valeur des cartes
                for (Map.Entry<Player, Card> entry : equalPlayedCards.entrySet()) {
                    //pour chaque potentiel gagnat de manche en égalité
                    Player player = entry.getKey();//récupération du joueur
                    try {
                        ObjectInputStream inputStream = new ObjectInputStream(playerSockets.get(player).getInputStream());
                        player.play(playerSockets.get(player));//joue la première carte du joueur
                        Card card = (Card) inputStream.readObject();//récupère la carte jouée
                        playedCards.add(card);
                        //la place face cachée et l'ajoute à la liste des cartes jouées
                        System.out.println(player.getName() + " lay a face-down card");
                        //affiche un message pour dire que le joueur a placé une carte face cachée
                        try {
                            player.play(playerSockets.get(player));//joue la première carte du joueur
                            card = (Card) inputStream.readObject();//récupère la carte jouée
                            card.setHidden(false);//la place face visible
                            playedCards.add(card);//l'ajoute à la liste des cartes jouées
                            System.out.println(player.getName() + " play " + card.getName());
                            //affiche un message pour dire que le joueur a joué une carte
                            if (card.getValue() > val) {//si la carte est la plus fotre
                                val = card.getValue();//modifie la valeur de val pour la valeur de la carte
                                winner = player;
                                //met temporairement le joueur l'ayant posé en tant que gagnant pour la manche
                                reEqualPlayedCards.clear();//vide reEqualPlayedCards
                                reEqualPlayedCards.put(player, card);
                                //met le potentiel gagnant de manche et sa dernière carte jouée dans
                                //equalPlayedCards
                            } else if (card.getValue() == val) {//en cas de nouvelle égalité
                                reEqualPlayedCards.put(player, card);
                                //rajoute le joueur à nouveau en égalité et sa carte
                                winner = null; //met le gagnant de manche comme indéfinit
                            }
                        } catch (IndexOutOfBoundsException e) {//si le joueur n'a plus de carte
                            System.out.println(player.getName() + " doesn't have a card to play");
                            //affiche un message pour dire que le joueur ne peut pa jouer d'autre carte
                        }
                    } catch (IndexOutOfBoundsException e) {//si le joueur n'a plus de carte
                        System.out.println(player.getName() + " doesn't have a card to play");
                        //affiche un message pour dire que le joueur ne peut pa jouer d'autre carte
                    }

                }
                equalPlayedCards.clear();//vide equalPlayedCards
                if (reEqualPlayedCards.size() != 1) { //si il y a eu à nouveau égalité
                    equalPlayedCards.putAll(reEqualPlayedCards);
                    //transfère reEqualPlayedCards dans equalPlayedCards
                }
            }

            for (Card card : playedCards) {//donne toutes les cartes jouées au gagnant
                card.setHidden(true);//les met face cachée
                winner.addComponent(card);//les ajoutes au gagnant
            }

            displayState();//affiche l'état de jeu
            int playersAbleToPlay = 0;//nombre de joueurs pouvant jouer
            for (Player player : players) {
                if (player.getScore() > 0) {//si le joueur a au moins une carte il peut jouer
                    playersAbleToPlay++;//incrémente le nombre de joueur pouvant jouer
                    matchWinner = (playersAbleToPlay == 1) ? player : null;
                    //si le joueur est le premier il est définit temporairement comme gagnant
                    //si un autre joueur peut jouer met le gagnant comme indéfinit
                }
            }
            if (playersAbleToPlay <= 1) endGame = true;
            // si il n'y a qu'un joueur pouvant jouer termine le jeu
        }
        if (matchWinner != null) matchWinner.clearHand();
        started = false;//réinitialise le jeu
        return matchWinner;//renvoie le gagnant
    }
}
