package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardGame extends CardBoard implements Game {
    String name;
    Integer maxPlayer;
    List<Player> players;
    boolean endGame, started;
    Board board;

    /**
     * Instancie un jeu
     *
     * @param name      nom du jeu
     * @param maxPlayer nombre maximum de joueurs
     * @param filename  nom du fichier source pour des données
     */
    public BoardGame(String name, Integer maxPlayer, String filename) {
        this.name = name;
        this.maxPlayer = maxPlayer;
        this.players = new ArrayList<>();
        endGame = false;
        started = false;
        initialize(filename);
    }

    /**
     * Ajoute un joueur si il n'est pas déjà en jeu ou si le nombre max de participants est atteint
     *
     * @param player
     * @return
     */
    public boolean addPlayer(Socket socket, Player player) throws IOException, ClassNotFoundException {

        if (player.isPlaying()){
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject("You already are in a game!");
            return false; // si le joueur est déjà en jeu
        }

        if (players.size() < maxPlayer) {

            for (Player joueur : players) {
                if (joueur.getName().compareTo(player.getName()) == 0) {
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject("Another player already have this username!");
                    return false; //si un joueur du même nom est dans le jeu
                }
            }
            players.add(player);
            player.canPlay(true);
            System.out.println(player.getName() + " join "+ this.name + "!");
            if(players.size() == maxNumberOfPlayers()) started = true;
            return true; // si le joueur a été ajouté
        }
        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        outputStream.writeObject("Max number of player already reached! (max : " + this.maxPlayer + ")");
        return false; // si le nombre maximum de participant est atteint
    }

    /**
     * Retire un joueur du jeu
     *
     * @param player joueur a retirer
     */
    public void removePlayer(Player player) {
        for (Player joueur : players) {
            if (joueur == player) {
                players.remove(player);
                player.canPlay(false);
                System.out.println(player.getName() + " left " + this.name);
                break;
            }
        }
    }

    /**
     * Retire tous les joueurs du jeu
     */
    public void removePlayers() {
        for (Player player : players) {
            System.out.println(player.getName() + " left " + this.name);
        }
        players.clear();
    }

    /**
     * Affiche l'état du jeu
     */
    public void displayState() {
        System.out.println("------------------------------------");
        System.out.println("------------ Game State ------------");
        System.out.println("------------------------------------");
        for (Player player : players) {
            System.out.println(player);
        }
        System.out.println("------------------------------------");
    }

    /**
     * Etat du jeu
     *
     * @return si le jeu est commencé ou non
     */
    public boolean isStarted() {
        return this.started;
    }

    /**
     * Nombre maximum de participants
     *
     * @return le nombre maximum de participants
     */
    public Integer maxNumberOfPlayers() {
        return this.maxPlayer;
    }

    /**
     * Liste les joueurs présents
     *
     * @return la liste des joueurs présents
     */
    public List<Player> getPlayers() {
        return players;
    }

    @Override
    public Player getCurrentPlayer(String username) {
        for (Player player : this.players){
            if (player.getName().compareTo(username) == 0) return player;
        }
        return null;
    }
}
