package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;


import java.io.*;
import java.net.Socket;
import java.util.*;

public class CardGame extends BoardGame {
    List<Card> cards;
    Integer numberOfRounds;

    /**
     * fait appel au construteur de BoardGame
     *
     * @param name       nom du jeu
     * @param maxPlayers nombre maximum de participant
     * @param filename   nom du fichier source des données
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
        board = new CardBoard();
    }

    /**
     * Créé le paquet de carte
     *
     * @param filename
     */
    @Override
    public void initialize(String filename) {
        this.cards = new ArrayList<>();
        try {
            File cardFile = new File(filename); //récupère le fichier
            Scanner myReader = new Scanner(cardFile); //commence la lecture
            while (myReader.hasNextLine()) { //lit le fichier
                String data = myReader.nextLine(); //récupère un ligne
                String card[] = data.split(";"); //découpe la ligne pour récupérer les données
                this.cards.add(new Card(card[0], Integer.parseInt(card[1]), true));
                //créé la carte associée aux données
            }
            myReader.close(); //termine la lecture
        } catch (FileNotFoundException e) { //si le fichier n'est pas trouvé
            System.out.println("An error occurred !");
            e.printStackTrace();
        }
    }

    /**
     * Lance la partie
     *
     * @return le gagnant du jeu
     * @param playerSockets
     */
    @Override
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        if (players.size() == 0 || players.size() == 1) return null;//si il n'y a pas assez de joueurs
        started = true; //jeu commencé
        Collections.shuffle(this.cards);//mélange le paquet
        Player winner = null; //vainqueur du jeu
        Player roundWinner = null; //vaiqueur de la manche
        numberOfRounds = 0; //numéro du tour
        int joueur = 0; //numéro de joueurs
        for (Card card : this.cards) { //distibue les cartes
            card.setPlayer(players.get(joueur));
            players.get(joueur).addComponent(card); //ajoute la carte du haut du paquet au joueur
            joueur = ++joueur % players.size(); //change de joueur
        }



        while (!end()) { //tant que le jeu n'est pas fini
            numberOfRounds++; //change de tour
            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject("Round " + numberOfRounds);//affiche le tour en cours
            }

            if (numberOfRounds % 10 == 0) {
                //mélange les cartes de chaque joueur en possédant tout les 10 tours
                for (Player player : players) {
                    if (player.getScore() != 0) player.shuffleHand();
                    //mélange les cartes du joueur si il en a
                }
            }

            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject(this);
            }

            Map<Player, Card> playedCard = new HashMap<>();

            //Liste des joueurs ayant joué et la carte qu'ils ont joué
            Integer val = 0;//variable de test pour la valeur des cartes
            for (Player player : players) {
                if (player.getScore() != 0) { //vérifie que le joueur a au moins une carte
                    for (Player player1 : players){
                        ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                        if(player1.equals(player)){
                            outputStream.writeObject("["+player1.getName()+"] you have to play ...");
                            ObjectOutputStream game = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                            game.writeObject(this);
                        }
                        else {
                            outputStream.writeObject("Waiting for "+player.getName()+" to play ...");
                        }
                    }
                    ObjectInputStream inputStream = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    Card card = (Card) inputStream.readObject();//récupère la carte jouée
                    player.removeComponent(card);
                    card.setHidden(false);//la met face visible
                    if (card.getValue() > val) { //si la carte est la plus forte
                        val = card.getValue();//modifie la valeur de val pour la valeur de la carte
                        roundWinner = player;
                        //met temporairement le joueur l'ayant posé en tant que gagnant pour la manche

                    } else if (card.getValue() == val && Math.random() > 0.5) {
                        //si il y a égalité et que le joueur a été sélectionné
                        val = card.getValue();//modifie la valeur de val pour la valeur de la carte
                        roundWinner = player;
                        //met temporairement le joueur l'ayant posé en tant que gagnant pour la manche
                    }
                    playedCard.put(player, card); //place la carte dans les cartes jouées
                    board.addComponent(card);
                    for (Player player1 : players){
                        ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                        outputStream.writeObject("Player " + player.getName() + " play " + card.getName());
                        //affiche un message pour dire que le joueur a joué sa carte
                    }

                }
            }

            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject(board);
            }

            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject("Player: "+roundWinner+" won the round with "+playedCard.get(roundWinner));
            }

            for (Map.Entry<Player, Card> entry : playedCard.entrySet()) {
                //donne les cartes jouées au gagnant de la mance
                Card card = entry.getValue(); //récupère une carte
                card.setPlayer(roundWinner);
                card.setHidden(true); //la met face cachée
                roundWinner.addComponent(card);//l'ajoute au joueur
            }

            int playersAbleToPlay = 0;//nombre de joueurs pouvant jouer
            for (Player player : players) {
                if (player.getScore() > 0) {//si le joueur a au moins une carte il peut jouer
                    playersAbleToPlay++;//incrémente le nombre de joueur pouvant jouer
                    winner = (playersAbleToPlay == 1) ? player : null;
                    //si le joueur est le premier il est définit temporairement comme gagnant
                    //si un autre joueur peut jouer met le gagnant comme indéfinit
                }
            }
            board.clear();
            if (playersAbleToPlay <= 1) {
                endGame = true;
                // si il n'y a qu'un joueur pouvant jouer termine le jeu
            }
        }
        if (winner != null) winner.clearHand();
        started = false;//réinitialise le jeu
        return winner;//renvoie le gagnant
    }

    /**
     * Si le jeu est fini ou non
     *
     * @return si le jeu est fini ou non
     */
    @Override
    public boolean end() {
        return this.endGame;
    }

    /**
     * Nom du jeu
     *
     * @return le nom du jeu
     */
    @Override
    public String toString() {
        return this.name;
    }
}
