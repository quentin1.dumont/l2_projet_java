package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame implements Game {

    private List<Card> cards;
    private List<Coin> coins;
    private int maxRounds, numberOfRounds;

    public PokerGame(Integer maxPlayer) {
        super("Poker", maxPlayer, "resources/games/pokerGame.txt");
        board = new PokerBoard();
        maxRounds = 3;
    }

    @Override
    public void initialize(String filename) {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
        try {
            File cardFile = new File(filename); //récupère le fichier
            Scanner myReader = new Scanner(cardFile); //commence la lecture
            while (myReader.hasNextLine()) { //lit le fichier
                String rawData = myReader.nextLine(); //récupère un ligne
                String data[] = rawData.split(";"); //découpe la ligne pour récupérer les données
                if (data[0].compareTo("Card") == 0) {
                    this.cards.add(new Card(data[1], Integer.parseInt(data[2]), true));
                    //créé la carte associée aux données
                } else if (data[0].compareTo("Coin") == 0) {
                    this.coins.add(new Coin(data[1], Integer.parseInt(data[2])));
                }
            }
            myReader.close(); //termine la lecture
        } catch (FileNotFoundException e) { //si le fichier n'est pas trouvé
            System.out.println("An error occurred !");
            e.printStackTrace();
        }
    }

    @Override
    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {
        if (players.size() == 0 || players.size() == 1) return null;//si il n'y a pas assez de joueurs
        Player winner = null;
        Player roundWinner = null;
        endGame = false;
        numberOfRounds = 0;
        for (Player player : players) {
            for (Coin coin : coins) {
                player.addComponent(coin);
            }
        }
        List<Player> playerUnableToPlay = new ArrayList<>();
        while (!end()) {
            numberOfRounds++;
            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject("Round " + numberOfRounds);//affiche le tour en cours
            }
            Collections.shuffle(cards);

            for (Player player : players){
                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                outputStream.writeObject(this);
            }

            for (int i = 0; i < 3 * (players.size() + 1); i++) {
                if (i < 3 * players.size()) {
                    players.get(i % players.size()).addComponent(cards.get(i));
                } else {
                    board.addComponent(cards.get(i));
                }
            }
            for (int i = 0; i < 3; i++) {
                for (Player player : players) {
                    if (player.getScore() == 0) playerUnableToPlay.add(player);
                    if (((players.size() - playerUnableToPlay.size()) == 1)
                            && !playerUnableToPlay.contains(player)) {
                        roundWinner = player;
                        break;
                    } else if (!playerUnableToPlay.contains(player)) {
                        for (Player player1 : players){
                            ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                            outputStream.writeObject(board);
                        }
                        for (Player player1 : players){
                            ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                            if(player1.equals(player)){
                                outputStream.writeObject("["+player1.getName()+"] you have to play ...");
                                ObjectOutputStream game = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                                game.writeObject(this);
                            }
                            else {
                                outputStream.writeObject("Waiting for "+player.getName()+" to play ...");
                            }
                        }
                        ObjectInputStream inputStream = new ObjectInputStream(playerSockets.get(player).getInputStream());
                        Coin coin = (Coin) inputStream.readObject();//récupère la carte jouée
                        if (coin != null) {
                            for (Player player1 : players){
                                ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                                outputStream.writeObject(player.getName() + " bet 1 " + coin.getName() + " coin");
                            }
                            player.removeComponent(coin);
                            board.addComponent(coin);
                        } else {
                            playerUnableToPlay.add(player);
                        }
                    }
                }
                for(Component component : board.getSpecificComponents(Card.class)){
                    Card card = (Card) component;
                    card.setHidden(false);
                }
            }
            if (roundWinner == null) {
                Integer winningFrequency = 0, winningValue = 0;
                for (Player player : players) {
                    Integer frequency = 1;
                    Integer cardValue = 0;
                    if (!playerUnableToPlay.remove(player)) {

                        //On récupére les valeurs des cartes des joueurs et du plateau
                        List<Component> mainPlayer = player.getSpecificComponents(Card.class);
                        List<Integer> playerHand = new ArrayList<>();
                        for (Component card : mainPlayer) playerHand.add(card.getValue());
                        for (Component carte : board.getSpecificComponents(Card.class)) {
                            playerHand.add(carte.getValue());
                        }
                        //Pour chaque valeur de cartes
                        for (Integer value : playerHand) {
                            //Récupèration du nombre d'apparition
                            int cardFrequency = Collections.frequency(playerHand, value);
                            //nombre d'apparitions maximum
                            if (cardFrequency > frequency) {
                                //on change le nombre d'apparitions et la valeur
                                frequency = cardFrequency;
                                cardValue = value;
                                //Si le nombre d'apparitionest le même mais que la valeur est supérieur
                            } else if (cardFrequency == frequency && value > cardValue) {
                                //on change juste la valeur
                                cardValue = value;
                            }
                        }
                        //Si il y a plus d'apparition chez le joueur que chez le gagnant
                        if (frequency > winningFrequency) {
                            //On prend les valeurs du nouveau gagnant
                            roundWinner = player;
                            winningFrequency = frequency;
                            winningValue = cardValue;
                        }//Sinon, si le nbr d'apparition est pareil mais que les valeurs des cartes est différente
                        else if (frequency.equals(winningFrequency) && cardValue > winningValue) {
                            //Le gagnant est bien le joueur et on change la valuer la plus haute
                            roundWinner = player;
                            winningValue = cardValue;
                            //Sinon joueur aléatoire
                        } else if (frequency.equals(winningFrequency) && cardValue.equals(winningValue) && Math.random() > 0.5) {
                            roundWinner = player;
                        }
                        for (Player player1 : players){
                            ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player1).getOutputStream());
                            outputStream.writeObject(player.getName() + " has " + frequency + " same card(s) of value " + cardValue);
                        }
                    }
                }
                for (Player player : players){
                    ObjectOutputStream outputStream = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                    outputStream.writeObject(roundWinner.getName() + " win with " + winningFrequency + " same card(s) of value " + winningValue);
                }
            }


            for (Component coin : board.getSpecificComponents(Coin.class)) {
                roundWinner.addComponent(coin);
            }
            board.clear();
            for (Player player : players) {
                player.clearHand();
            }
            playerUnableToPlay.clear();
        }


        int score = 0;
        for (Player player : players) {
            if (player.getScore() > score) {
                score = player.getScore();
                winner = player;
            } else if (player.getScore() == score && Math.random() > 0.5) {
                winner = player;
            }
        }
        return winner;
    }

    @Override
    public boolean end() {
        if (numberOfRounds == maxRounds) endGame = true;
        return this.endGame;
    }

    @Override
    public String toString(){
        return this.name;
    }
}
