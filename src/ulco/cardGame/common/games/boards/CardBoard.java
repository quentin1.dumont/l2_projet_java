package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {
    private List<Card> cards;

    public CardBoard(){
        cards = new ArrayList<>();
    }

    @Override
    public void clear() {
        cards.clear();
    }

    @Override
    public void addComponent(Component component) {
        if(component instanceof Card) cards.add((Card) component);
    }

    @Override
    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Class.class) return new ArrayList<>(cards);
        return null;
    }

    @Override
    public void displayState() {
        System.out.println("----------- Board State -----------");
        for (Card card : cards) {
            if (!card.isHidden()) System.out.println(card + " played by "+card.getPlayer().getName());
        }
        System.out.println("-----------------------------------");
    }
}
