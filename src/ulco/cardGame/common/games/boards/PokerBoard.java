package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class PokerBoard implements Board {
    List<Card>cards;
    List<Coin>coins;

    public PokerBoard(){
        cards = new ArrayList<>();
        coins = new ArrayList<>();
    }

    @Override
    public void clear() {
        cards.clear();
        coins.clear();
    }

    @Override
    public void addComponent(Component component) {
        if (component.getClass() == Card.class){
            cards.add((Card) component);
        } else if (component.getClass() == Coin.class){
            coins.add((Coin) component);
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class){
            return new ArrayList<>(cards);
        } else if (classType == Coin.class){
            return new ArrayList<>(coins);
        }
        return null;
    }

    @Override
    public void displayState() {
        System.out.println("----------- Board State -----------");
        System.out.print("Cards :\t");
        for (Card card : cards) {
            if (!card.isHidden()) System.out.print(card + " ");
        }
        System.out.println();
        System.out.print("Coins :\t");
        int red = 0, blue = 0, black = 0;
        for (Coin coin : coins) {
            if (coin.getName().compareTo("Red")==0){
                red++;
            } else if (coin.getName().compareTo("Blue")==0){
                blue++;
            } else if (coin.getName().compareTo("Black")==0){
                black++;
            }
        }
        System.out.println("Red coins : "+red+", Blue coins : "+blue+", Black coins : "+black);
        System.out.println("-----------------------------------");
    }
}
