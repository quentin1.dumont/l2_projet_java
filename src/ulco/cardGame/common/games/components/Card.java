package ulco.cardGame.common.games.components;

public class Card extends Component {
    private boolean hidden;

    /**
     * Créé une carte
     *
     * @param name   nom de la carte
     * @param value  valeur de la carte
     * @param hidden visibilité de la carte
     */
    public Card(String name, Integer value, boolean hidden) {
        super(name, value);
        this.hidden = hidden;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
