package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    String name;
    boolean playing;
    Integer score;

    /**
     * Initialise le joueur avec un score de 0 et n'étant pas en jeu
     *
     * @param name nom du joueur
     */
    public BoardPlayer(String name) {
        this.name = name;
        this.score = 0;
        this.playing = false;
    }

    /**
     * Permet de changer la disponibilité du joueur
     *
     * @param playing disponibilité du joueur (en jeu ou pas)
     */
    public void canPlay(boolean playing) {
        this.playing = playing;
    }

    /**
     * nom du joueur
     *
     * @return le nom du joueur
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * disponibilité du joueur
     *
     * @return si le joueur est en jeu ou pas
     */
    @Override
    public boolean isPlaying() {
        return playing;
    }

    /**
     * Statut du joueur
     *
     * @return le statut du joueur
     */
    @Override
    public String toString() {
        String msg = (this.isPlaying()) ? " (Playing) score : " + score : " (Free)";
        return "BoardPlayer " + name + msg;
    }
}
