package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BoardPlayer {
    List<Card> cards;

    /**
     * Créé un joueur de carte
     *
     * @param name nom du joueur
     */
    public CardPlayer(String name) {
        super(name);
        cards = new ArrayList<>();
    }

    /**
     * Score du joueur
     *
     * @return le nombre de cartes du joueur
     */
    public Integer getScore() {
        return this.score;
    }

    /**
     * Joue la première carte du joueur
     *
     * @return la première carte du joueur
     */
    @Override
    public void play(Socket socket) throws IOException {
        if (this.score > 0) {
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(this.cards.remove(0));
        }
    }

    /**
     * Ajoute une ou plusieurs carte(s) au joueur
     *
     * @param component une carte ou une liste de cartes
     */
    @Override
    public void addComponent(Component component) {
        this.cards.add((Card) component);
        this.score++;
    }

    /**
     * Retire une carte précise du joueur
     *
     * @param component La carte à retirer
     */
    @Override
    public void removeComponent(Component component) {
        score--;
        this.cards.remove(component);
    }

    /**
     * liste les cartes du joueur
     *
     * @return la liste des cartes du joueur
     */
    @Override
    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class) return new ArrayList<>(cards);

        return null;
    }
    /**
     * Mélange les cartes du joueur
     */
    @Override
    public void shuffleHand() {
        Collections.shuffle(this.cards);
    }

    /**
     * retire toutes les cartes du joueur
     */
    @Override
    public void clearHand() {
        this.cards.clear();
    }

    @Override
    public void displayHand(){
        System.out.println(this.name + " : " + this.score + " cards");
    }

    /**
     * statut du joueur
     *
     * @return le statut du joueur
     */
    @Override
    public String toString() {
        String msg = (this.isPlaying()) ? " (Playing) score : " + score : " (Free)";
        return "CardPlayer " + name + msg;
    }
}
