package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer implements Player {
    private List<Card>cards;
    private List<Coin>coins;

    public PokerPlayer(String name) {
        super(name);
        cards = new ArrayList<>();
        coins = new ArrayList<>();
    }

    @Override
    public Integer getScore() {
        return score;
    }

    @Override
    public void play(Socket socket) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        String color = "";
        while (true) {
            System.out.println("Bet : 1 -> Red coin, 2 -> Blue coin, 3 -> Black coin, other -> fold");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            if (choice == 1) {
                color = "Red";
            } else if (choice == 2) {
                color = "Blue";
            } else if (choice == 3) {
                color = "Black";
            } else {
                outputStream.writeObject(null);
                return;
            }

            for (Coin coin : coins) {
                if (coin.getName().compareTo(color) == 0) {
                    outputStream.writeObject(coin);
                    return;
                }
            }
            System.out.println("No " + color + "coin found!");
        }
    }

    @Override
    public void addComponent(Component component) {
        if (component instanceof Card){
            cards.add((Card) component);
        } else if (component instanceof Coin){
            coins.add((Coin) component);
            score += component.getValue();
        }
    }

    @Override
    public void removeComponent(Component component) {
        if (component instanceof Card){
            cards.remove((Card) component);
        } else if (component instanceof Coin){
            Coin coinToRemove = null;
            score -= component.getValue();
            for (Coin coin : coins){
                if(coin.getName().compareTo(component.getName()) == 0){
                    coinToRemove = coin;
                    break;
                }
            }
            coins.remove(coinToRemove);
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class){
            return new ArrayList<>(cards);
        } else if (classType == Coin.class){
            return new ArrayList<>(coins);
        }
        return null;
    }

    @Override
    public void shuffleHand() {
        Collections.shuffle(cards);
    }

    @Override
    public void displayHand(){
        System.out.println(this.name + " :");
        System.out.print("Cards :\t");
        for (Card card :cards) {
            System.out.print(card.getName() + " ");
        }
        System.out.println();
        System.out.print("Coins :\t");
        int red = 0, blue = 0, black = 0;
        for (Coin coin : coins) {
            if (coin.getName().compareTo("Red")==0){
                red++;
            } else if (coin.getName().compareTo("Blue")==0){
                blue++;
            } else if (coin.getName().compareTo("Black")==0){
                black++;
            }
        }
        System.out.println("Red coins : "+red+", Blue coins : "+blue+", Black coins : "+black);
        System.out.println("Score : " + this.getScore());
    }

    @Override
    public void clearHand() {
        cards.clear();
    }

    public String toString() {
        String msg = (this.isPlaying()) ? " (Playing) score : " + score : " (Free)";
        return "PokerPlayer " + name + msg;
    }
}
