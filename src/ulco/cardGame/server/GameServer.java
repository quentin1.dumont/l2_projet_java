package ulco.cardGame.server;

import ulco.cardGame.common.games.BattleGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class GameServer {

    /**
     * Test des jeux
     *
     * @param args vide pour l'instant
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Game game = null;
        Player player1 = null;
        Player player2 = null;
        Player player3 = null;
        Player player4 = null;
        boolean valid = false;
        while (!valid) {
            System.out.println("Choose a game : 0 -> Battle, 1 -> Poker");
            int choice = scanner.nextInt();
            if(choice == 0){
                game = new BattleGame(4);
                player1 = new CardPlayer("Gregory");
                player2 = new CardPlayer("Florian");
                player3 = new CardPlayer("Jerome");
                player4 = new CardPlayer("Bilal");
                valid = true;
            } else if (choice == 1) {
                game = new PokerGame(4);
                player1 = new PokerPlayer("Gregory");
                player2 = new PokerPlayer("Florian");
                player3 = new PokerPlayer("Jerome");
                player4 = new PokerPlayer("Bilal");
                valid = true;
            }
        }
        System.out.println(player1);
        System.out.println(player2);
        System.out.println(player3);
        System.out.println(player4);
        /*game.addPlayer(player1);
        game.addPlayer(player2);
        game.addPlayer(player3);
        game.addPlayer(player4);
        Player winner = game.run(playerSockets);
        String msg = (winner != null) ? winner.getName() + " wins !!!" : "No winner!";
        System.out.println(msg);*/
        game.removePlayers();
    }
}

